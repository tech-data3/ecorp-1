import pandas as pd
import numpy as np
import mysql.connector
from sqlalchemy import create_engine, text

def create_engine_wrapper(db):
    """
    wrapper for the sqlalchemy engine

    :param db: str database name
    :return: salalchemy engine
    """
    username = ""
    password = ""
    host = "127.0.0.1"
    return  create_engine("mysql+pymysql://{user}:{pw}@{host}/{db}".format(
        user=username,
        pw=password,
        host=host,
        db=db
        )
    )
if __name__ == "__main__":
    db_output_db = "ecorp"
    db_input_db = "exo"

    # create table from sql file from data and retrieve data
    engine = create_engine_wrapper(db_input_db) 
    with engine.connect() as con :
        with open("data/department.sql") as file:
            queries = file.read().split(';');
        for query in queries:
            try:
                con.execute(text(query))
            except Exception:
                print("query skipped : ", query)
        department=pd.read_sql("SELECT * FROM department", con, index_col="dept_num")
        query = text("DROP TABLE IF EXISTS department")
        con.execute(query)
    engine.dispose()

    department.rename(columns={'dept_num' : 'id', 'dept_name' : 'name'}, inplace=True)
    # we group departement to drop doublon and keep the list of the old id
    department = department.groupby(by="name").apply(lambda x: list(x.index)).to_frame()
    department.rename(columns={0 : 'old_id'}, inplace = True)
    department.reset_index(inplace=True)
    department.index+=1
    # we exlode the groupby departement dataframe to get the relation between old and new id
    department = department.explode('old_id')
    id_transition = department.loc[:,['old_id']]
    id_transition.reset_index(inplace=True)
    id_transition.rename(columns={'index':'new_id'}, inplace=True)
    # we can drop doublons now
    department = department.loc[:,['name']].drop_duplicates()

    employees=pd.read_csv("data/employees.csv", index_col="id")
    employees.rename(columns={'job':'department_id'}, inplace=True)
    employees = pd.merge(
            employees,
            id_transition,
            how='left',
            left_on='department_id', right_on='old_id'
            ).drop(columns=['old_id', 'department_id'])
    employees.rename(columns={'new_id':'department_id'}, inplace=True)
    employees.index+=1

    products=pd.read_excel("data/products.xlsx", engine="openpyxl", index_col=None)

    products_date=pd.read_json("data/products_date.json", orient='records')

    products_table = products.join(products_date)
    products_table.drop(columns='product_id', inplace=True)
    products_table.rename(columns={'max_date':'date_max'}, inplace=True)

    engine = create_engine_wrapper(db_output_db) 

    department.to_sql('department', engine, if_exists='append', index_label='id')
    id_transition.to_sql('old_id_dept', engine, if_exists='append', index=False)
    employees.to_sql('employee', engine, if_exists='append', index_label='id')
    products_table.to_sql('product', engine, if_exists='append', index=False)

    engine.dispose()
