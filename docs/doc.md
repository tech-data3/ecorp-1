# ECORP-1

https://apricot-bat-0d7.notion.site/ECORP-1-5f7fef4595b34ca5b185c3686efcba49

## Installation

```bash
git clone https://gitlab.com/tech-data3/ecorp-1.git
cd ecorp-1

mkdir data
cd data
git clone https://gitlab.com/Nasyourte/ecorp-1 .
sudo mysql < init.sql
sudo mysql -e "GRANT ALL PRIVILEGES ON ecorp.* TO '<username>'@'localhost';"
sudo mysql -e "CREATE DATABASE IF NOT EXISTS exo"; 
sudo mysql -e "GRANT ALL PRIVILEGES ON exo.* TO '<username>'@'localhost';"
cd ..
python3 ecorp.py
```

## What we deal with ?
![schema des donnees que nous avons](wehave.png)

### department.sql
department(dept_name, dept_num)

doublons

### employees.csv
employees(id,first_name,last_name,email,job)

en retirant les doublons il y aura des id de la colonne job a changer

### products_date.json
products_date(product_id, max_date)

relation 1 to 1 avec les donnees de products du coup on peut les mettre dans les meme tables

### products.xlsx
products(name, price, available, description)

pas d'id (numero de ligne comme id ?)

## what we want

## MLD
![schema de la bdd](schema.png)
