DROP DATABASE IF EXISTS ecorp;

CREATE DATABASE ecorp;

use ecorp;

CREATE TABLE department (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL UNIQUE
);

CREATE TABLE old_id_dept(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    old_id INT NOT NULL,
    new_id INT NOT NULL,
    FOREIGN KEY (new_id) REFERENCES department(id) ON DELETE CASCADE
);

CREATE TABLE employee(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    first_name VARCHAR(25) NOT NULL,
    last_name VARCHAR(25) NOT NULL,
    email VARCHAR(256) NOT NULL UNIQUE,
    department_id INT,
    FOREIGN KEY (department_id) REFERENCES department(id) ON DELETE SET NULL
);


CREATE TABLE product(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    price DECIMAL NOT NULL,
    available BOOLEAN NOT NULL,
    description TEXT,
    date_max DATE
);
